/* ---------------------------------------------------------------------
	Global Js
	Target Browsers: All

	HEADS UP! This script is for general functionality found on ALL pages and not tied to specific components, blocks, or
	plugins.


	If you need to add JS for a specific block or component, create a script file in js/components or js/blocks and
	add your JS there. (Don't forget to enqueue it!)
------------------------------------------------------------------------ */
var FX = (function (FX, $) {
    /**
     * Doc Ready
     *
     * Use a separate $(function() {}) block for each function call
     */
    $(() => {
        FX.General.init(); // For super general or super short scripts
    })

    $(() => {
        FX.Affix.init(); // Header Sticky Nav
    })

    $(() => {
        FX.ExternalLinks.init(); // Enable by default
    })

    $(() => {
        FX.MobileMenu.init();
    })

    $(() => {
        FX.Search.init();
    })

    $(() => {
        FX.Accordion.init();
    })

    $(() => {
        FX.Footer.init();
    })

    $(window).on('load', () => {
        FX.BackToTop.init()
    })

    /**
     * Example Code Block - This should be removed
     * @type {Object}
     */
    FX.CodeBlock = {
        init() {},
    };

    /**
     * General functionality — ideal for one-liners or super-duper short code blocks
     */
    FX.General = {
        init() {
            this.bind();
        },

        bind() {
            // Makes all PDF to open in new tabs
            $('a[href*=".pdf"]').each(e => {
                $(this).attr('target', '_blank');
            });
            // FitVids - responsive videos
            $('body').fitVids();
            // Input on focus remove placeholder
            $('input,textarea').focus(() => {
                $(this).removeAttr('placeholder');
            });
        }
    };

    /**
     * Affix
     * Fixes sticky items on scroll
     * @type {Object}
     */
    FX.Affix = {
        $body: null,
        $header: null,
        headerHeight: null,
        scrollFrame: null,
        resizeFrame: null,

        init() {
            this.$body = $(document.body);
            this.$header = $('#page-header');
            this.headerHeight = this.$header.outerHeight(true);
            this.bind();
        },

        bind(e) {
            $(window).on('scroll', this.handleScroll.bind(this));
            $(window).on('resize', this.handleResize.bind(this));
        },

        handleScroll(e) {
            var self = this;
            // avoid constantly running intensive function(s) on scroll
            if (null !== self.scrollFrame) {
                cancelAnimationFrame(self.scrollFrame)
            }
            self.scrollFrame = requestAnimationFrame(self.maybeAffixHeader.bind(self))
        },

        handleResize(e) {
            var self = this;
            // avoid constantly running intensive function(s) on resize
            if (null !== self.resizeFrame) {
                cancelAnimationFrame(self.resizeFrame)
            }
            self.resizeFrame = requestAnimationFrame(() => {
                self.headerHeight = self.$header.outerHeight(true);
            })
        },

        maybeAffixHeader() {
            var self = this;
            if (0 < $(window).scrollTop()) {

				if( $(window).width() > 768 ) {
	                self.$header.addClass('page-sticky');
				}
            } else {

				if( $(window).width() > 768 ) {
	                self.$header.removeClass('page-sticky');
				}
            }
        }
    };

    /**
     * Force External Links to open in new window.
     * @type {Object}
     */
    FX.ExternalLinks = {
        init() {
            if ('string' === typeof FX.siteurl) {
                var siteUrlBase = FX.siteurl.replace(/^https?:\/\/((w){3})?/, '');
                $('a[href*="//"]:not([href*="' + siteUrlBase + '"])').not('.ignore-external') // ignore class for excluding
                    .addClass('external').attr('target', '_blank').attr('rel', 'noopener');
            }
        }
    };

    /**
     * Mobile menu script for opening/closing menu and sub menus
     * @type {Object}
     */
    FX.MobileMenu = {
        init() {
            this.$toggle = $('.js-mobile');
            this.$menu = $('.js-dropdown');
            this.$search = $('.js-search');
            this.$searchform = $('.nav-form');
            this.bind();

            $('.nav-primary li.menu-item-has-children > a').after('<span class="sub-menu-toggle icon-dropdown"></span>');

            $('.sub-menu-toggle').click(() => {
                var $this = $('.sub-menu-toggle'),
                    $parent = $this.closest('li'),
                    $wrap = $parent.find('> .sub-menu');
                $parent.toggleClass('js-toggled');
                $wrap.toggleClass('js-toggled');
                $this.toggleClass('js-toggled icon-dropdown icon-dropdown-active');
            });
        },

        bind(e) {
            var self = this;
            $(self.$toggle).click(() => {
                self.$toggle.find('.icon').toggleClass('icon-menu icon-close');
                self.$menu.toggleClass('active');
                self.$toggle.toggleClass('active');
                // For Search Close
                self.$search.addClass('icon-search icon-close');
                self.$search.removeClass('icon-close');
                self.$searchform.removeClass('active');
            });
        }
    };

    /**
     * Search script
     * @type {Object}
     */
    FX.Search = {
        init() {
            this.$search = $('.js-search');
            this.$searchform = $('.nav-form');
            this.$toggle = $('.js-mobile');
            this.$menu = $('.js-dropdown');
            this.bind();
        },

        bind(e) {
            var self = this;
            $(self.$search).click(function(e) {
                e.preventDefault();

                if( $(window).width() < 1999 ) {
                    self.$search.toggleClass('icon-search icon-close icon-hide');
                } else {
                    self.$search.toggleClass('icon-search icon-close');
                }

                self.$searchform.toggleClass('active');
                self.$searchform.find('input').focus();
                self.$toggle.find('.icon').addClass('icon-menu');
                self.$toggle.find('.icon').removeClass('icon-close');
                self.$menu.removeClass('active');
                self.$toggle.removeClass('active');
            });
        }
    };

    /**
     * Homepage Accordion Script
     * @type {Object}
     */
    FX.Accordion = {

        init: function () {
            this.bind();
            $('.accordion-content').hide();
        },

        bind: function () {
            $('.accordion-toggle > .accordion-title').on('toggle-panel', this.togglePanel);
            $('.accordion-toggle > .accordion-title').click(this.togglePanel);
        },

        togglePanel: function () {
            $this = $(this);
            $target = $this.parent().next();
            if ($this.hasClass('active')) {
                $this.removeClass('active');
                $target.removeClass('active').slideUp();
            } else {
                $('.accordion-content').removeClass('active').slideUp();
                $('.accordion-title').removeClass('active');
                $this.addClass('active');
                $target.addClass('active').slideDown();
            }
        }
    };

    /**
     * Footer Contact Scripts
     * @type {Object}
     */
    FX.Footer = {
        init() {
            this.$group = $('.form-group');
            this.bind();
        },

        bind(e) {
            var self = this;

            $(document).click(function(){
                setTimeout(function(){
                    $(self.$group).each(function(){
                        if( $(this).find('label').hasClass('active')) {

                            if( $(this).find('input').val() || $(this).find('input').is(":focus") ) {

                            } else {
                                $(this).find('label').removeClass('active');
                            }
                        }
                    });
                }, 50);

            });

            $(self.$group).click(function(e) {
                $(this).find('label').addClass('active');
                $(this).find('input').focus();
            });
        }
    };

    /**
     * Display scroll-to-top after a certain amount of pixels
     * @type {Object}
     */
    FX.BackToTop = {
        $btn: null,

        init() {
            this.$btn = $('.back-to-top');
            if (this.$btn.length) {
                this.bind();
            }
        },

        bind() {
            $(window).on('scroll load', this.maybeShowButton.bind(this));
            this.$btn.on('click', this.scrollToTop);
        },

        maybeShowButton() {
            if ($(window).scrollTop() > 100) { // TODO: Update "100" for how far down page to show button
                this.$btn.removeClass('hide');
            } else {
                this.$btn.addClass('hide');
            }
        },

        scrollToTop() {
            $(window).scrollTop(0);
        }
    };

    /**
     * FX.SmoothAnchors
     * Smoothly Scroll to Anchor ID
     * @type {Object}
     */
    FX.SmoothAnchors = {
        hash: null,

        init() {
            this.hash = window.location.hash;
            if ('' !== this.hash) {
                this.scrollToSmooth(this.hash);
            }
            this.bind();
        },

        bind() {
            $('a[href^="#"]').on('click', $.proxy(this.onClick, this));
        },

        onClick(e) {
            e.preventDefault();
            var target = $(e.currentTarget).attr('href');
            this.scrollToSmooth(target);
        },

        scrollToSmooth(target) {
            var $target = $(target),
                headerHeight = 0 // TODO: if using sticky header change to $('#page-header').outerHeight(true)
            $target = ($target.length) ? $target : $(this.hash);
            if ($target.length) {
                var targetOffset = $target.offset().top - headerHeight;
                $('html, body').animate({
                    scrollTop: targetOffset
                }, 600);
                return false;
            }
        }
    };

    return FX;

}(FX || {}, jQuery));
