var FX = (function (FX, $) {
    $(() => {
        FX.LogoSlider.init()
    })
    FX.LogoSlider = {
        $slider: null,
        init() {
            this.$slider = $('.logo-slider-wrapper')
            if (this.$slider.length) {
                this.applySlick()
            }
        },
        applySlick() {
            this.$slider.slick({
                dots: false,
                autoplay: true,
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							prevArrow: '<button class="slide-right icon-right"></button>',
							nextArrow: '<button class="slide-left icon-left"></button>',
						}
					},
				]
            });
        }
    }
    return FX
}(FX || {}, jQuery))
