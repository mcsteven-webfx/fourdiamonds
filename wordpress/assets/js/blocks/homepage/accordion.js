    /**
	* Accordion
	 */
        FX.Accordion = {
            ​
            init: function() {
                this.bind();
                    $('.accordion-content').hide();
            },
            ​
            bind: function() {
                $('.accordion-toggle > .accordion-title').on( 'toggle-panel', this.togglePanel );
                $('.accordion-toggle > .accordion-title').click( this.togglePanel );
            },
            ​
            togglePanel: function() {
                $this = $(this);
                $target =  $this.parent().next();
            ​
                if ( $this.hasClass('active') ) {
                    $this.removeClass('active');
                    $target.removeClass('active').slideUp();
                    } else {
                    $('.accordion-content').removeClass('active').slideUp();
                    $('.accordion-title').removeClass('active');
                    $this.addClass('active');
                    $target.addClass('active').slideDown();
                    }
                }
            };

