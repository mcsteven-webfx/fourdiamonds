/**
 * Homepage Fundraiser JS
 */
var FX = ( function( FX, $ ) {
    $(() => {
     FX.HomepageFundraiser.init();
    })

    FX.HomepageFundraiser = {
        init() {
            this.$toggle = $('.js-fundraiser');
            this.bind();
        },
        bind(e) {
            var self = this;
            $(self.$toggle).click(function(e) {
                if( $(document).width() < 768 ) {
                    $(self.$toggle).removeClass('active');
                    $(this).addClass('active');
                }
            });
        },
    };

    return FX

} ( FX || {}, jQuery ) )
