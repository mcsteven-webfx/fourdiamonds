/**
 * Homepage About JS
 */
var FX = ( function( FX, $ ) {
    $(() => {
     FX.HomepageAbout.init();
    })

    FX.HomepageAbout = {
        init() {
            this.$toggle = $('.js-about-toggle');
            this.$allitems = $('.about-item');
            this.$alldetails = $('.about-info');
            this.bind();
        },
        bind(e) {
            var self = this;
            $(self.$toggle).click(function(e) {
                self.$parent = $(this).parents('.about-item');
                self.$target = $(this).data('toggle');

                self.$allitems.removeClass('active');
                self.$parent.addClass('active');

                self.$allitems.find('.icon').removeClass('icon-minus').addClass('icon-plus');
                self.$parent.find('.icon').addClass('icon-minus').removeClass('icon-plus');

                self.$alldetails.removeClass('active');
                $('[data-detail="'+self.$target+'"]').addClass('active');
            });
        },
    };

    return FX

} ( FX || {}, jQuery ) )
