var FX = ( function( FX, $ ) {

	/**
	 * Doc Ready
	 *
	 * Use a separate $(function() {}) block for each function call
	 */
	$( () => {

        // FitVids - responsive videos
        $('.js-video--fitvids').fitVids();
        FX.VideoCover.init()
    })

    /**
	 * Video cover on click
	 * @type {Object}
	 */
	FX.VideoCover = {
        $videoCoverLink: null,

		init() {
            this.$videoCoverLink = $('.js-video-text-cover__link');
            this.$videoCoverLink.on('click', function(e){
                e.preventDefault();
                let video = $(this).attr('href').split('#')[1];

                $(this).closest('.video-text-cover').hide();
                if(video === "youtube") {
                    let iframe = $(this).closest('.video-text__video').find('iframe');
                    let url = $(iframe).attr('src') + '?autoplay=1';
                    $(iframe).attr('src', url);
                } else {
                    let player = $(this).closest('.video-text__video').find('video');
                    player.get(0).play()
                }
            });
		},
	};

    return FX;

} ( FX || {}, jQuery ) );
