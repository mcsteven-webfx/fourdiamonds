var FX = ( function( FX, $ ) {

	$( () => {
		FX.TeamSlider.init()
	})

	FX.TeamSlider = {
		$slider: null,

		init() {
			this.$slider = $('.js-team-member-slider')
			this.$slider.on('init', function () {
                var slideHeight = $('.js-team-member-slider .slick-track').height();
                $('.js-team-member-slider .slick-slide').css('height', slideHeight + 'px');
            });

			if( this.$slider.length ) {
				this.applySlick()
			}
		},

		applySlick() {
            this.$slider.slick( {
                mobileFirst: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                nextArrow: '<i class="icon icon-Left"></i>',
                prevArrow: '<i class="icon icon-Right"></i>',
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                ]
            });
		}
	}

	return FX

} ( FX || {}, jQuery ) )
